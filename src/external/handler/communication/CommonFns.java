package external.handler.communication;

import com.mollatech.rewardme.connector.communication.EmailConnectorSettings;
import java.util.Date;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;

public class CommonFns {

    public static String g_strMessage;
    public static String g_sendOTPEmail;

//    static {
//
//        String filepath = "email-OTP-as-doc.txt";
//
//        g_strMessage = LoadMessage();
//
//        g_sendOTPEmail = LoadTemplate(filepath, g_sendOTPEmail);
//
//    }
//
//    private static String LoadTemplate(String filepath, String into) {
//        try {
//            BufferedReader br = new BufferedReader(new FileReader(filepath));
//            StringBuilder sb = new StringBuilder();
//            String line = br.readLine();
//            int count = 0;
//
//            while (line != null) {
//                count++;
//                sb.append(line);
//                sb.append("\n");
//                line = br.readLine();
//            }
//
//            String everything = sb.toString();
//            into = everything;
//            br.close();
//            return into;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    private static String LoadMessage() {
//
//        return g_strMessage;
//
//    }
//    public boolean SendOTPEmail(String useremail, String strSubject, String message,String [] cc, String [] bcc, String[] filepaths, String[] mimetypes, EmailConnectorSettings emailSettings) {
//        g_strMessage = message;
//
//        try {
//
//            String host = emailSettings.smtpIp;
//
//            String port = String.valueOf(emailSettings.port);
//
//            String username = emailSettings.userId;
//            String passwd = emailSettings.password;
//            boolean bSMTPAuth = emailSettings.authRequired;
//            boolean bSSLEnabled = emailSettings.ssl;
//            String fromAddress = emailSettings.fromEmail;
//            String fromName = emailSettings.fromName;
//            //String url = (String) emailSettings.getReserve2();
//
//            String strBody = g_sendOTPEmail;
//            strBody = strBody.replaceAll("#email#", useremail);
//            strBody = strBody.replaceAll("#OTP#", message);
//            //String strSubject = strSubject;
//
//            boolean bCC = false;
//            if ( cc != null || bcc != null ) {
//                bCC = true;
//            }
//
//            for (int i = 0; i < 1; i++) {
//
//                SendEmailUtil s = new SendEmailUtil(
//                        host, port, username, passwd, bSMTPAuth, bSSLEnabled,
//                        strBody, strSubject, fromAddress, fromName,
//                        useremail, useremail, bCC, filepaths, mimetypes, cc, bcc);
//                try {
//                    s.start();
//                } catch (Exception e10) {
//                    e10.printStackTrace();
//                }
//            }
//        } catch (Exception e) {
//            return false;
//        }
//        return true;
//
//    }
    public boolean copyfile(String srFile, String dtFile) {
        try {
            File f1 = new File(srFile);
            File f2 = new File(dtFile);
            InputStream in = new FileInputStream(f1);

            OutputStream out = new FileOutputStream(f2);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
            //System.out.println("File copied.");
            return true;
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + " in the specified directory.");
            return false;
        }
    }

    private String bytesToHex(byte[] b) {
        char hexDigit[] = {'0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        StringBuffer buf = new StringBuffer();
        for (int j = 0; j < b.length; j++) {
            buf.append(hexDigit[(b[j] >> 4) & 0x0f]);
            buf.append(hexDigit[b[j] & 0x0f]);
        }
        return buf.toString();
    }

    public String GetCode(int ilength) {
        try {
            Date d = new Date();
            String strRegCodeSecret = d.toString();
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(strRegCodeSecret.getBytes());
            byte[] output = md.digest();
            byte[] half_output = new byte[ilength];
            System.arraycopy(output, 0, half_output, 0, ilength);
            String strHexSecret = bytesToHex(half_output);
            //System.out.println("hex value:" + strHexSecret);
            return strHexSecret;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] SHA(String message) {
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            byte[] array = sha1.digest(message.getBytes());
            return array;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean SendEmail(String useremail, String strSubject, String strBody,
            String[] cc, String[] bcc, String[] filepaths,
            String[] mimetypes, EmailConnectorSettings emailSettings,
            String msgid) {
        //g_strMessage = message;

        try {

            String host = emailSettings.smtpIp;
            String port = String.valueOf(emailSettings.port);
            String username = emailSettings.userId;
            String passwd = emailSettings.password;
            boolean bSMTPAuth = emailSettings.authRequired;
            boolean bSSLEnabled = emailSettings.ssl;
            String fromAddress = emailSettings.fromEmail;
            String fromName = emailSettings.fromName;

            boolean bCardonCopyNeeded = false;
            if (cc != null || bcc != null) {
                bCardonCopyNeeded = true;
            }

            for (int i = 0; i < 1; i++) {

                SendEmailUtil s = new SendEmailUtil(
                        host, port, username, passwd, bSMTPAuth, bSSLEnabled,
                        strBody, strSubject, fromAddress, fromName,
                        useremail, useremail, bCardonCopyNeeded, filepaths, mimetypes, cc, bcc, msgid);
                try {
                    boolean result = s.SendEmail();
                    //s.start();
                    return result;
                } catch (Exception e10) {
                    e10.printStackTrace();
                }
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}
