package external.handler.communication;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;

public class SendEmailUtil extends Thread {

    private String host = null;
    private String port = null;
    private String userid = null;
    private String password = null;
    private boolean bSMTPAuth = false;
    private boolean bSSLEnabled = false;
    private String strBody = null;
    private String strSubject = null;
    private String fromAddress = null;
    private String fromName = null;
    private String toAddress = null;
    private String toName = null;
    private boolean bCC = false;
    private String[] ccAddresses = null;
    private String[] bccAddresses = null;
    private String[] nameFiles = null;
    private String[] mimeTypes = null;
    private String messageid = null;
    // private boolean status = false;

    // private String nameFiles = null;
    public SendEmailUtil(String host,
            String port,
            String userid,
            String password,
            boolean bSMTPAuth,
            boolean bSSLEnabled,
            String strBody,
            String strSubject,
            String fromAddress,
            String fromName,
            String toAddress,
            String toName,
            boolean bCardonCopyNeeded,
            String[] nameFiles,
            String[] mimeTypes,
            String[] ccAddresses,
            String[] bccAddresses,
            String msgid
    ) {

        this.host = host;
        this.port = port;
        this.userid = userid;
        this.password = password;
        this.bSMTPAuth = bSMTPAuth;
        this.bSSLEnabled = bSSLEnabled;
        this.strBody = strBody;
        this.strSubject = strSubject;
        this.fromAddress = fromAddress;
        this.fromName = fromName;
        this.toAddress = toAddress;
        this.toName = toName;
        this.bCC = bCardonCopyNeeded;
        this.ccAddresses = ccAddresses;
        this.bccAddresses = bccAddresses;
        this.nameFiles = nameFiles;
        this.mimeTypes = mimeTypes;
        this.messageid = msgid;
    }

//    public SendEmailUtil(String host, String port, String username, String passwd, boolean bSMTPAuth,
//            boolean bSSLEnabled, String strBody, String strSubject, String fromAddress, String fromName,
//            String[] mgremails, String[] mgremails0,
//            boolean bCC, Object object, Object object0, Object object1) {
//        throw new UnsupportedOperationException("Not yet implemented");
//    }
    @Override
    public void run() {
//        this.status = 
        SendEmail();
        //System.out.println(bResult);
    }

    public boolean SendEmail() {

        try {
            java.util.Properties props = new java.util.Properties();
            props.setProperty("mail.smtp.host", host);
            props.setProperty("mail.smtp.port", port); // for google smtp
            if (bSMTPAuth == true) {
                props.setProperty("mail.smtp.auth", "true");
            }

            if (bSSLEnabled == true) {
                props.put("mail.smtp.starttls.enable", "true");
            }

            Session mailsession = null;

            if (bSMTPAuth == true) {
                Authenticator authenticator = new Authenticator(userid, password);
                props.setProperty("mail.smtp.submitter", authenticator.getPasswordAuthentication().getUserName());
                mailsession = Session.getInstance(props, authenticator);
            } else {
                mailsession = Session.getInstance(props);
            }

            Address address = new InternetAddress(toAddress, toName);
            Message message = new MimeMessage(mailsession);
            message.setHeader("MESSAGE-ID", messageid);
            message.addRecipient(Message.RecipientType.TO, address);
            if (bCC == true) {
//                if (ccAddresses == null || bccAddresses == null ) 
                if (ccAddresses == null && bccAddresses == null) {
                    return false;
                }
                if (ccAddresses != null) {
                    for (String ccAddresse : ccAddresses) {
                        Address addressCC = new InternetAddress(ccAddresse, ccAddresse);
                        message.addRecipient(Message.RecipientType.CC, addressCC);
                    }
                }

                if (bccAddresses != null) {
                    for (String bccAddresse : bccAddresses) {
                        Address addressBCC = new InternetAddress(bccAddresse, bccAddresse);
                        message.addRecipient(Message.RecipientType.BCC, addressBCC);
                    }
                }
            }
            message.setFrom(new InternetAddress(fromAddress, fromName));
            message.setSubject(strSubject);
            message.setSentDate(new java.util.Date());

            Multipart multipart = new MimeMultipart();
            MimeBodyPart messagePart = new MimeBodyPart();
            messagePart.setContent(strBody, "text/html");
            multipart.addBodyPart(messagePart);

            if (nameFiles != null && nameFiles.length > 0) {
                //int j = 0;
                int i = 0;
                for (i = 0; i < nameFiles.length; i++) {
                    if (nameFiles != null) {
//                        MimeBodyPart attachmentPart = new MimeBodyPart();
//                        FileDataSource fileDataSource = new FileDataSource(nameFiles[i]);/*{
//                            @Override
//                            public String getContentType() {
//                                return mimeTypes[j];
//                            }
//                        };*/
//                        attachmentPart.setDataHandler(new DataHandler(fileDataSource));
//                        attachmentPart.setFileName(fileDataSource.getName());
//                        multipart.addBodyPart(attachmentPart);     
                        MimeBodyPart messageBodyPart = new MimeBodyPart();
                        FileDataSource file = new FileDataSource(nameFiles[i]);
                        DataSource source = file;
                        //  DataSource source = new FileDataSource(fileAttachment[i]);
                        messageBodyPart.setDataHandler(new DataHandler(source));
                        messageBodyPart.setFileName(file.getName());
                        //messageBodyPart.setContent("", "application/pkix-cert");
                        multipart.addBodyPart(messageBodyPart);
                    }
                }
            }
            message.setContent(multipart);
            Transport.send(message);
            return true;

        } catch (Exception ex) {
            ex.printStackTrace();;
            //System.out.println("SendEmail Exception = " + ex.getMessage());
            return false;
        }

    }
//

    private class Authenticator extends javax.mail.Authenticator {

        private PasswordAuthentication authentication;

        public Authenticator(String username, String password) {
            authentication = new PasswordAuthentication(username, password);
        }

        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
            return authentication;
        }
    }
}
