package external.handler.communication;

import com.mollatech.rewardme.connector.communication.MobileConnectorSettings;
import com.mollatech.rewardme.connector.communication.RMStatus;
import com.mollatech.rewardme.connector.communication.SMSSender;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.SmsFactory;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Sms;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class SMSHandlerTwilio implements SMSSender {

    static MobileConnectorSettings smsSetting = null;
    static String smsUserId = null;
    static String smsPassword = null;
    static String smsPhonenumber = null;

    @Override
    public RMStatus Load(MobileConnectorSettings setting) {

        RMStatus axiomstatus = new RMStatus();

        smsSetting = setting;
        if (smsSetting != null) {
            smsUserId = smsSetting.userid;
            smsPassword = smsSetting.password;
            smsPhonenumber = smsSetting.phoneNumber;
            axiomstatus.iStatus = 0;
            axiomstatus.strStatus = "Loaded";
            //smsSetting = null;
            return axiomstatus;
        }
        return null;
    }

    @Override
    public RMStatus Unload() {
        RMStatus axiomstatus = new RMStatus();

        if (smsSetting != null) {
            axiomstatus.iStatus = 0;
            axiomstatus.strStatus = "unloaded";
            return axiomstatus;

        }

        axiomstatus.iStatus = 1;
        axiomstatus.strStatus = "error";
        return axiomstatus;
    }

    @Override
    public String SendMessage(String number, String message) {

        //SMSHandlerTwilio twilio = new SMSHandlerTwilio();
        // SMSSetting setting = new SMSSetting();

        /* Instantiate a new Twilio Rest Client */
        TwilioRestClient client = new TwilioRestClient(smsUserId, smsPassword);

        // Get the account and call factory class
        Account acct = client.getAccount();
        SmsFactory smsFactory = acct.getSmsFactory();

        String fromNumber = smsPhonenumber;

        //build map of post parameters 
        Map<String, String> params = new HashMap<String, String>();
        params.put("From", fromNumber);
        params.put("To", number);
        params.put("Body", message);

        try {
            // send an sms a call  
            // ( This makes a POST request to the SMS/Messages resource)
            Sms sms = smsFactory.create(params);
//                System.out.println("Success sending SMS: " + sms.getSid());
//                System.out.println("SMS :" + sms.getBody());

            String msgId = sms.getSid();
            return msgId;
        } catch (TwilioRestException e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    public RMStatus GetStatus(String msgid) {

        TwilioRestClient client = new TwilioRestClient(smsUserId, smsPassword);

        Sms sms = client.getAccount().getSms(msgid);

        RMStatus status = new RMStatus();
        //  System.out.println(call.getStatus() + " " + call.getSid());

        if (sms.getStatus().equals("queued")) {
            status.iStatus = 1;
            status.strStatus = "queued";
            return status;
        } else if (sms.getStatus().equals("sending")) {
            status.iStatus = 2;
            status.strStatus = "sending";
            return status;
        } else if (sms.getStatus().equals("sent")) {
            status.iStatus = 0;
            status.strStatus = "completed";
            return status;
        } else if (sms.getStatus().equals("failed")) {
            status.iStatus = 4;
            status.strStatus = "failed";
            return status;
        } else if (sms.getStatus().equals("received")) {
            status.iStatus = 0;
            status.strStatus = "completed";
            return status;
        }
        return null;
    }

    @Override
    public RMStatus GetServiceStatus() {
        int result = -1;
        RMStatus aStatus = new RMStatus();
        aStatus.iStatus = result;
        aStatus.strStatus = "FAILED";
        try {
            URL url = new URL("http://" + smsSetting.ip + ":" + smsSetting.port);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // System.out.println(conn.getResponseCode());
            int code = conn.getResponseCode();
            if (code == 200) {
                result = 0;
                aStatus.iStatus = result;
                aStatus.strStatus = "SUCCESS";
                return aStatus;
            } else {
                return aStatus;
            }
        } catch (Exception ex) {
            //Logger.getLogger(SMSHandlerTwilio.class.getName()).log(Level.SEVERE, null, ex);
            aStatus.iStatus = -2;
            aStatus.strStatus = "EXCEPTION::" + ex.getMessage();
            return aStatus;
        }
    }
}
