package external.handler.communication;

import com.mollatech.rewardme.connector.communication.EmailConnectorSettings;
import com.mollatech.rewardme.connector.communication.EmailSender;
import com.mollatech.rewardme.connector.communication.RMStatus;
import java.security.MessageDigest;
import java.util.Date;
import java.util.Properties;
import java.util.regex.Pattern;
import javax.mail.Transport;

public class EmailImplementation implements EmailSender {

    static EmailConnectorSettings emailSetting = null;
    static final int queued = 1;
    static final int ringing = 2;
    static final int failed = 3;
    static final int busy = 4;
    static final int canceled = 5;
    static final int completed = 6;
    static final int inProgress = 7;
    static final int noAnswer = 8;
    boolean strStatus;

    @Override
    public RMStatus Load(EmailConnectorSettings setting) {
        RMStatus axiomstatus = new RMStatus();
        emailSetting = setting;
        if (emailSetting != null) {

            setting = null;
            return axiomstatus;

        }
        return axiomstatus;

    }

    @Override
    public RMStatus Unload() {
        RMStatus axiomstatus = new RMStatus();

        if (emailSetting != null) {
            return axiomstatus;

        }
        return axiomstatus;
    }

    @Override
    public RMStatus GetStatus(String msgid) {

        RMStatus status = new RMStatus();
        status.iStatus = -1;
        status.strStatus = "failed";
        if (msgid != null) {
            //   int i = msgid.indexOf(":");
            //   String strStatus = msgid.substring(0, i);
            if (this.strStatus == true) {
                status.iStatus = 0;
                status.strStatus = "completed";
            } else {

                status.iStatus = -1;
                status.strStatus = "failed";
            }
        }
        return status;

    }

    @Override
    public String SendMessage(String emailid, String subject, String message, String[] cc,
            String[] bcc, String[] files,
            String[] mimetypes) {
        Date d = new Date();
        String msgid = emailid + subject + message + d.toString();
        byte[] SHA1hash = SHA1(msgid);
        String sid = asHex(SHA1hash);

        Pattern rfc2822 = Pattern.compile("^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");

        if (rfc2822.matcher(emailid).matches() == false) {
            return null;
        }
        CommonFns cFns = new CommonFns();
        boolean bResult = cFns.SendEmail(emailid, subject, message, cc, bcc, files, mimetypes, emailSetting, sid);
        this.strStatus = bResult;
        if (bResult == true) {
            return sid;
        } else {
            //return null;
            return sid;
        }
    }

    private byte[] SHA1(String message) {
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            byte[] array = sha1.digest(message.getBytes());
            return array;
        } catch (Exception e) {
            return null;
        }
    }

    public String asHex(byte buf[]) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        int i;

        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10) {
                strbuf.append("0");
            }

            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }

        return strbuf.toString();
    }

    @Override
    public RMStatus GetServiceStatus() {
        int result = -1;
        RMStatus aStatus = new RMStatus();
        aStatus.iStatus = result;
        aStatus.strStatus = "FAILED";
        try {
            Properties props = new Properties();
            if (emailSetting.authRequired == true) {
                props.setProperty("mail.smtp.auth", "true");
            } else {
                props.setProperty("mail.smtp.auth", "false");
            }
            if (emailSetting.ssl == true) {
                props.setProperty("mail.smtp.startssl.enable", "true");
            } else {
                props.setProperty("mail.smtp.starttls.enable", "true");
            }
            javax.mail.Session session = javax.mail.Session.getInstance(props, null);
            Transport transport = session.getTransport("smtp");
            // int portint = Integer.parseInt(port);
            transport.connect(emailSetting.smtpIp, emailSetting.port, emailSetting.userId, emailSetting.password);
            transport.close();
            result = 0;
            aStatus.iStatus = result;
            aStatus.strStatus = "SUCCESS";
        } catch (Exception e) {
            //e.printStackTrace();
            // return aStatus;
        }
        return aStatus;
    }

//    public static void SendEmailWithAttachment() {
//
//        String SMTP_HOST_NAME = "smtp.gmail.com";
//        String SMTP_PORT = "587";
//        String SMTP_FROM_ADDRESS = "vikramsareen@gmail.com";
//        String SMTP_TO_ADDRESS = "vikram@mollatech.com";
//        String subject = "email with attachment";
//        //String fileAttachment = "/Users/vikramsareen/Desktop/all/amk/SG-Protect-2-Mobile-Trust-APIS.pdf";
//        //String fileAttachment = "/Users/vikramsareen/Downloads/FindLocation.war";        
//        //String fileAttachment = "/Applications/NetBeans/apache-tomcat-7.0.27/bin/axiomv2-settings/dbsetting.conf";
//        //String fileAttachment = "/Applications/NetBeans/apache-tomcat-7.0.27/bin/axiomv2-settings/localhost.pfx";
//        String fileAttachment = "/Users/vikramsareen/Desktop/69.46.75.142.cer";
//        
//
//        Properties props = new Properties();
//
//        props.put("mail.smtp.host", SMTP_HOST_NAME);
//        props.put("mail.smtp.auth", "true");
//        props.put("mail.debug", "true");
//        props.put("mail.smtp.port", SMTP_PORT);
//        props.setProperty("mail.smtp.starttls.enable", "true");
//        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
//            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
//                //return new javax.mail.PasswordAuthentication("AKIAISFL7JEVLXRIA4XQ", "AmpeMf1VmxbG4UC5xTWhfJGhEGKcdzYB3Ic+59sj4FBP");
//                return new javax.mail.PasswordAuthentication("vikramsareen@gmail.com", "29092009");
//            }
//        });
//        try {
//
//            Message msg = new MimeMessage(session);
//
//            msg.setFrom(new InternetAddress(SMTP_FROM_ADDRESS));
//            // create the message part 
//            MimeBodyPart messageBodyPart =
//                    new MimeBodyPart();
//            //fill message
//            messageBodyPart.setText("Test mail one");
//            Multipart multipart = new MimeMultipart();
//            multipart.addBodyPart(messageBodyPart);
//            // Part two is attachment
//            messageBodyPart = new MimeBodyPart();
//            DataSource source = new FileDataSource(fileAttachment);
//            messageBodyPart.setDataHandler(new DataHandler(source));
//            messageBodyPart.setFileName(fileAttachment);
//            multipart.addBodyPart(messageBodyPart);
//            // Put parts in message
//            msg.setContent(multipart);
//
//
//            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(SMTP_TO_ADDRESS));
//
//            msg.setSubject(subject);
//            // msg.setContent(content, "text/plain");
//
//            Transport.send(msg);
//            //System.out.println("success....................................");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//    
//    public static void main(String [] args){
//        SendEmailWithAttachment();
//    }
}
